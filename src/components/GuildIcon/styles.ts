import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  image: {
    height: 67,
    width: 63,
    borderRadius: 8,
  },
});
